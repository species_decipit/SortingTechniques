import java.util.ArrayList;

/**
 * Implementation of Selection Sort
 */
public class SelectionSort {
    public static void selectionSort(ArrayList<Integer> array) {
        for (int i = 0; i < array.size() - 1; i++) {
            int min = array.get(i);
            int indexOfMin = i;
            for (int j = i; j < array.size(); j++) {                // Find max element
                if (array.get(j) < min) {
                    min = array.get(j);
                    indexOfMin = j;
                }
            }
            SupportMethods.swap(array, i, indexOfMin);              // Swap max element with i-th element
        }
    }
}
